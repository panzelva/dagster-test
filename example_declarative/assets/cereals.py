
import csv
from dagster import asset, Output, OpExecutionContext
import requests
import pandas as pd


@asset
def cereals(context: OpExecutionContext):
    """All cereals in the world"""

    response = requests.get("https://docs.dagster.io/assets/cereal.csv")
    lines = response.text.split("\n")
    cereals = [row for row in csv.DictReader(lines)]
    return Output(
        pd.DataFrame(cereals),
        metadata={
            "count": len(cereals)
        },
    )


@asset
def nabisco_cereals(context: OpExecutionContext, cereals: pd.DataFrame):
    """Cereals manufactured by Nabisco"""

    nabisco = cereals[cereals['mfr'] == "N"]
    return Output(
        nabisco,
        metadata={
            "count": len(nabisco)
        },
    )


# @asset(required_resource_keys={"bigquery"}, group_name="bigquery")
# def table_data(context: v) -> pd.DataFrame:
    # query = """
    #     SELECT * FROM `nano-green-1.beniktest.dddd`
    # """

    # rows = context.resources.bigquery.query(query)
    # return rows.to_dataframe()


# @asset(required_resource_keys={"bigquery"}, group_name="bigquery")
# def table_data_production_mean(context: OpExecutionContext, table_data: pd.DataFrame) -> Float:
#     mean = table_data['production'].mean()

#     context.log_event(
#         AssetObservation(
#             asset_key="table_data_production_mean", metadata={"mean": mean}
#         )
#     )

#     return mean
