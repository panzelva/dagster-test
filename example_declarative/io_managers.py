from dagster import IOManager, io_manager
import pandas as pd
from google.cloud import bigquery


class BigQuertIOManager(IOManager):
    def handle_output(self, context, obj: pd.DataFrame):
        dataset, table = context.asset_key.path[-2], context.asset_key.path[-1]
        client = bigquery.Client()
        client.load_table_from_dataframe(
            obj,
            f"nano-green-1.{dataset}.{table}"
        )

    def load_input(self, context):
        dataset, table = context.asset_key.path[-2], context.asset_key.path[-1]
        query = f"""
            SELECT * FROM `nano-green-1.{dataset}.{table}`
        """

        client = bigquery.Client()
        rows = client.query(query)
        return rows.to_dataframe()


@io_manager
def bigquery_io_manager(_):
    return BigQuertIOManager()
