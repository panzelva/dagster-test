from dagster import repository, with_resources, ScheduleDefinition, define_asset_job, load_assets_from_modules
from example_declarative.io_managers import bigquery_io_manager
from .assets import cereals
from dagster_gcp import bigquery_resource


@repository()
def repository_declarative():
    return [
        *with_resources(
            definitions=[
                *load_assets_from_modules(
                    modules=[cereals],
                    group_name="cereals",
                    key_prefix="zelviktest"
                )
            ],
            resource_defs={
                "bigquery": bigquery_resource,
                "io_manager": bigquery_io_manager
            }
        ),
        ScheduleDefinition(
            job=define_asset_job("all_assets", selection="*"),
            cron_schedule="@daily"
        ),
    ]
