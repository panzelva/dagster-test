from random import randint
import requests
import csv
from dagster import DynamicOut, DynamicOutput, job, op, repository, AssetObservation, OpExecutionContext, Int, Float, ScheduleDefinition, resource, String, AssetMaterialization, InitResourceContext, Field, Output
from dagster_k8s import k8s_job_executor
import psycopg2
import pandas as pd
from io import StringIO
from dagster_gcp.gcs.io_manager import gcs_pickle_io_manager
from dagster_gcp.gcs.resources import gcs_resource
from contextlib import contextmanager

# ===========================
# basic
# ===========================

 
@op(config_schema={"weight_pounds": Int})
def pounds_to_kilograms(context: OpExecutionContext) -> Float:
    return context.op_config["weight_pounds"] / 2.2046


@op(config_schema={"height_inches": Int})
def inches_to_metres(context: OpExecutionContext) -> Float:
    return context.op_config["height_inches"] * 0.0254


@op
def calculate_bmi(context: OpExecutionContext, weight_kilograms: Float, height_meters: Float) -> Float:
    bmi = weight_kilograms * height_meters
    context.log.info(f"BMI: {bmi}")
    return bmi


@op
def bmi_weight(context: OpExecutionContext, bmi: Float):
    if bmi < 18.5:
        context.log.info("nevim")
    else:
        context.log.info("nevim 2")


@job(config={
    "ops": {
        "inches_to_metres": {"config": {"height_inches": 75}},
        "pounds_to_kilograms": {"config": {"weight_pounds": 190}}
    }
})
def bmi():
    kilograms = pounds_to_kilograms()
    metres = inches_to_metres()
    bmi = calculate_bmi(kilograms, metres)
    bmi_weight(bmi)


bmi_schedule = ScheduleDefinition(job=bmi, cron_schedule="*/10 * * * *")


# ===========================
# chunkování
# ===========================


@op(out=DynamicOut())
def fetch_and_chunk_csv_data():
    response = requests.get(
        "https://www.stats.govt.nz/assets/Uploads/Annual-enterprise-survey/Annual-enterprise-survey-2021-financial-year-provisional/Download-data/annual-enterprise-survey-2021-financial-year-provisional-csv.csv")

    chunksize = 2000
    with pd.read_csv(StringIO(response.text), chunksize=chunksize) as reader:
        for idx, chunk in enumerate(reader):
            yield DynamicOutput(chunk.to_csv(index=False), mapping_key=str(idx))


@op()
def read_chunks(context: OpExecutionContext, csvText):
    lines = csvText.split("\n")
    rows = [row for row in csv.DictReader(lines)]

    context.log_event(
        AssetObservation(asset_key="annual_enterprise_survey",
                         metadata={"count": len(rows)})
    )


@job
def chunk_csv():
    parts = fetch_and_chunk_csv_data()
    parts.map(read_chunks)


@job(
    executor_def=k8s_job_executor,
    resource_defs={
        "gcs": gcs_resource,
        "io_manager": gcs_pickle_io_manager,
    },
    config={
        "resources": {
            "io_manager": {
                "config": {
                    "gcs_bucket": "nano-dev-dagster",
                    "gcs_prefix": "dagster",
                }
            }
        }
    },
)
def chunk_csv_k8s():
    parts = fetch_and_chunk_csv_data()
    parts.map(read_chunks)


# ===========================
# with postgres
# ===========================

@op(config_schema={"table_name": String}, required_resource_keys={"database"})
def create_table(context: OpExecutionContext) -> String:
    table_name = context.op_config['table_name']
    sql = f"CREATE TABLE IF NOT EXISTS {table_name} (column_1 VARCHAR(100));"
    context.resources.database.execute_query(sql)
    return table_name


@op(required_resource_keys={"database"})
def insert_into_table(context: OpExecutionContext, table_name: String) -> None:
    sql = f"INSERT INTO {table_name} (column_1) VALUES (1);"

    number_of_rows = randint(1, 10)
    for _ in range(number_of_rows):
        context.resources.database.execute_query(sql)
        context.log.info("Inserted row.")

    yield AssetMaterialization(
        asset_key="inserted_rows",
        description="Inserted random rows",
        metadata={
            "table_name": table_name,
            "number_of_rows": number_of_rows
        }
    )

    yield Output(number_of_rows)


class Postgres:
    def __init__(self, host: str, user: str, password: str, database: str, port: int):
        self._connection = psycopg2.connect(
            dbname=database,
            user=user,
            password=password,
            host=host,
            port=port
        )

    def execute_query(self, query: str):
        with self._connection:
            with self._connection.cursor() as curs:
                curs.execute(query)

    def cleanup(self):
        self._connection.close()


@resource(
    config_schema={
        "host": Field(String),
        "user": Field(String),
        "password": Field(String),
        "database": Field(String),
        "port": Field(Int)
    }
)
@contextmanager
def postgres_resource(context: InitResourceContext):
    context.log.info("Created database resource.")

    """This resource defines a psycopg2 connection"""
    database = Postgres(
        database=context.resource_config["database"],
        user=context.resource_config["password"],
        password=context.resource_config["password"],
        host=context.resource_config["host"],
        port=context.resource_config["port"]
    )

    try:
        yield database
    finally:
        context.log.info("Cleaned up database resource.")
        database.cleanup()


@job(
    resource_defs={"database": postgres_resource},
    config={
        "resources": {
            "database": {
                "config": {
                    "host": "localhost",
                    "user": "postgres",
                    "password": "postgres",
                    "database": "postgres",
                    "port": 5441
                }
            }
        },
        "ops": {"create_table": {"config": {"table_name": "test_table"}}},
    })
def postgres():
    table = create_table()
    insert_into_table(table)


table_schedule = ScheduleDefinition(job=postgres, cron_schedule="*/20 * * * *")


@repository
def repository_imperative():
    return [
        chunk_csv,
        chunk_csv_k8s,
        bmi,
        postgres
    ]
